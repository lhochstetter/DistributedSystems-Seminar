package consensusSimulator;

import java.util.LinkedList;
import java.util.List;

import consensusSimulator.AbstractNode.Pair;
import consensusSimulator.AbstractNode.State;

public class Overwatch {

	public static Scenario currentScenario;

	public static int	nodeCount		= 25;
	public static int	simulationRuns	= 10;
	public static StringBuilder ss = new StringBuilder();

	public static void main(String[] args) {

		Scenario delayed = new Scenario();
		delayed.MAX_COMMIT_TRIES = 100000;
		delayed.DESIRED_COMMITS = 1;
		delayed.NODE_COORDINATOR_TIMEOUT = 100;
		delayed.NODE_PARTICIPANT_TIMEOUT = 100;
		delayed.NODE_VOTE_ABORT_RATE = 0.f;
		delayed.NODE_MIN_CALCULATION_TIME = 0;
		delayed.NODE_MAX_CALCULATION_TIME = 1;
		delayed.NODE_CRASH_RATE = 0.f;
		delayed.NODE_CRASH_COUNT = 0;
		delayed.NODE_MIN_CRASH_TIME = 0;
		delayed.NODE_MAX_CRASH_TIME = 1;
		delayed.MSG_MIN_DELAY = 30;
		delayed.MSG_MAX_DELAY = 60;
		delayed.generateValuePool();

		Scenario crashing = new Scenario();
		crashing.MAX_COMMIT_TRIES = 100000;
		crashing.DESIRED_COMMITS = 1;
		crashing.NODE_COORDINATOR_TIMEOUT = 100;
		crashing.NODE_PARTICIPANT_TIMEOUT = 100;
		crashing.NODE_VOTE_ABORT_RATE = 0.f;
		crashing.NODE_MIN_CALCULATION_TIME = 0;
		crashing.NODE_MAX_CALCULATION_TIME = 1;
		crashing.NODE_CRASH_RATE = 0.01f;
		crashing.NODE_CRASH_COUNT = nodeCount;
		crashing.NODE_MIN_CRASH_TIME = 120;
		crashing.NODE_MAX_CRASH_TIME = 120;
		crashing.MSG_MIN_DELAY = 0;
		crashing.MSG_MAX_DELAY = 1;
		crashing.generateValuePool();

		Scenario aborting = new Scenario();
		aborting.MAX_COMMIT_TRIES = 100000;
		aborting.DESIRED_COMMITS = 1;
		aborting.NODE_COORDINATOR_TIMEOUT = 100;
		aborting.NODE_PARTICIPANT_TIMEOUT = 100;
		aborting.NODE_VOTE_ABORT_RATE = 0.1f;
		aborting.NODE_MIN_CALCULATION_TIME = 0;
		aborting.NODE_MAX_CALCULATION_TIME = 1;
		aborting.NODE_CRASH_RATE = 0.f;
		aborting.NODE_CRASH_COUNT = 0;
		aborting.NODE_MIN_CRASH_TIME = 0;
		aborting.NODE_MAX_CRASH_TIME = 1;
		aborting.MSG_MIN_DELAY = 0;
		aborting.MSG_MAX_DELAY = 1;
		aborting.generateValuePool();

		List<Scenario> scenarios = new LinkedList<>();
		scenarios.add(crashing);
		scenarios.add(delayed);
		scenarios.add(aborting);

		for (Scenario s : scenarios) {

			currentScenario = s;
			for (int i = 0; i < simulationRuns; i++)
				singleRun();
			ss.append("\n");
		}
		System.out.println(ss);
	}

	public static void printLogs() {
		StringBuilder s = new StringBuilder();

		s.append(String.format("%-6s | ", "Commit"));

		for (AbstractNode n : Interconnect.getNodes()) {
			if (0 == n.id)
				s.append(String.format("%-25s | ", "Coordinator"));
			else
				s.append(String.format("%-25s | ", "Participant " + n.id));
		}
		System.out.println(s);

		Pair<State, Object> p;
		for (int i = 0; i < Interconnect.getCoordinator().currentCommitID; i++) {
			s.setLength(0); // "clear" the StringBuilder
			s.append(String.format("%6d | ", i));
			for (AbstractNode n : Interconnect.getNodes()) {
				p = n.log.get(i);
				if (null == p) {
					s.append(String.format("%-6s %18s | ", "N/A", "N/A"));
					continue;
				}
				s.append(String.format("%-6s %18s | ", p.a, p.b));
			}
			System.out.println(s);
		}
	}

	public static void singleRun() {
		// System.out.println(currentScenario);
		// System.out.println("\n");
		// System.out.println(String.format("%5s | %6s | %-18s %-18s | %-9s | %-10s |
		// %s", "Node", "Commit", "Proposed",
		// "Current", "Event", "State", "Debug message"));
		// System.out.println(
		// "------------------------------------------------------------------------------------------------------------------------");
		//
		Interconnect.setup(1 + nodeCount);
		Interconnect.start();

		while ((Statistics.total_commits.get() < currentScenario.MAX_COMMIT_TRIES)
				&& (Statistics.successful_commits.get() < currentScenario.DESIRED_COMMITS)) {
			synchronized (Statistics.total_commits) {
				try {
					Statistics.total_commits.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		Interconnect.terminateGraceful();

		// Statistics.print();
		ss.append(Statistics.total_commits.get() + ";");
		currentScenario.generateValuePool();
		Statistics.reset();
		Interconnect.reset();

		// printLogs();
	}

	public static Scenario perfect(int n) {
		Scenario s = new Scenario();
		s.MAX_COMMIT_TRIES = n;
		s.DESIRED_COMMITS = n;
		s.NODE_COORDINATOR_TIMEOUT = 1000;
		s.NODE_PARTICIPANT_TIMEOUT = 1000;
		s.NODE_VOTE_ABORT_RATE = 0;
		s.NODE_MIN_CALCULATION_TIME = 0;
		s.NODE_MAX_CALCULATION_TIME = 1;
		s.NODE_CRASH_RATE = 0.f;
		s.NODE_CRASH_COUNT = 0;
		s.NODE_MIN_CRASH_TIME = 0;
		s.NODE_MAX_CRASH_TIME = 1;
		s.MSG_MIN_DELAY = 0;
		s.MSG_MAX_DELAY = 1;
		s.generateValuePool();
		return s;
	}

	public static Scenario reasonable(int n, int c) {
		Scenario s = new Scenario();
		s.MAX_COMMIT_TRIES = n;
		s.DESIRED_COMMITS = c;
		s.NODE_COORDINATOR_TIMEOUT = 1000;
		s.NODE_PARTICIPANT_TIMEOUT = 1000;
		s.NODE_VOTE_ABORT_RATE = 0.001;
		s.NODE_MIN_CALCULATION_TIME = 5000;
		s.NODE_MAX_CALCULATION_TIME = 20000;
		s.NODE_CRASH_RATE = 0.0001f;
		s.NODE_CRASH_COUNT = 1;
		s.NODE_MIN_CRASH_TIME = 100;
		s.NODE_MAX_CRASH_TIME = 1000;
		s.MSG_MIN_DELAY = 200;
		s.MSG_MAX_DELAY = 1000;
		s.generateValuePool();
		return s;
	}

}
