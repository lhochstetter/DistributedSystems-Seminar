package consensusSimulator;

public class Message {

	enum MessageType {
		DECISION, //
		COMMIT_REQUEST, // node to coordinator message to request a commit
		VOTE_REQUEST, // coordinator to all nodes requesting a vote for a
						// proposed commit
		VOTE_COMMIT, // node to coordinator voting for a proposed commit
		VOTE_ABORT, // node to coordinator voting against a proposed commit
		GLOBAL_ABORT, // coordinator to all nodes confirming proposed commit
		GLOBAL_COMMIT, // coordinator to all nodes aborting proposed commit
		ACK; // node to coordinator acknowledging GLOBAL_COMMIT / _ABORT
	}

	int				commitID;
	AbstractNode	sender;
	AbstractNode	receiver;
	MessageType		type;
	Object			payload;

	/**
	 * 
	 * @param s
	 *            = sender
	 * @param r
	 *            = receiver
	 * @param id
	 *            = commitId
	 * @param t
	 *            = message type
	 */
	Message(AbstractNode s, AbstractNode r, int id, MessageType t) {
		this.sender = s;
		this.receiver = r;
		this.commitID = id;
		this.type = t;
		this.payload = null;
	}

	/**
	 * 
	 * @param s
	 *            = sender
	 * @param r
	 *            = receiver
	 * @param id
	 *            = commitId
	 * @param t
	 *            = message type
	 * @param p
	 *            = payload
	 */
	Message(AbstractNode s, AbstractNode r, int id, MessageType t, Object p) {
		this.sender = s;
		this.receiver = r;
		this.commitID = id;
		this.type = t;
		this.payload = p;
	}

	@Override
	public String toString() {
		switch (type) {
		case COMMIT_REQUEST:
			return "MESSAGE Commit " + commitID + " from " + sender.id + " to " + receiver.id + " type " + type
					+ " content " + (Integer) payload;
		case DECISION:
			return "MESSAGE Commit " + commitID + " from " + sender.id + " to " + receiver.id + " type " + type
					+ " content " + (AbstractNode.State) payload;
		case VOTE_REQUEST:
			return "MESSAGE Commit " + commitID + " from " + sender.id + " to " + receiver.id + " type " + type
					+ " content " + (Integer) payload;
		default:
			return "MESSAGE Commit " + commitID + " from " + sender.id + " to " + receiver.id + " type " + type;
		}
	}
}