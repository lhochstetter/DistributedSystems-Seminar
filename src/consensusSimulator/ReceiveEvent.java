package consensusSimulator;

import consensusSimulator.Message.MessageType;

public class ReceiveEvent extends AbstractEvent {
	Message message;

	public ReceiveEvent(Message message) {
		super(message.receiver, EventType.RECEIVE);
		this.message = message;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(super.toString());
		s.append(String.format("%-14s %6d %4d ", message.type, message.commitID, message.sender.id));

		if (MessageType.COMMIT_REQUEST == message.type) s.append((String) message.payload);
		if (MessageType.DECISION == message.type) s.append((AbstractNode.State) message.payload);
		if (MessageType.VOTE_REQUEST == message.type) s.append((String) message.payload);
		if (null == message.payload) s.append(' ');

		s.append(String.format("[%d]", message.receiver.id));

		return s.toString();
	}
}
