package consensusSimulator;

import java.util.concurrent.ThreadLocalRandom;

import consensusSimulator.InternalEvent.InternalEventType;
import consensusSimulator.Message.MessageType;

public class Participant extends AbstractNode {
	private int		askNodeID;
	private State	preFailState;
	public String	currentValue;
	public String	proposedValue;

	Participant(int id, int currentCommitID) {
		super(id, currentCommitID);
		state = State.INIT;
		askNodeID = 1;
		currentValue = "";
		proposedValue = "";
	}

	@Override
	void send(SendEvent e) {
		Interconnect.send(e.sender, e.receivers, e.commitID, e.type, e.payload);
		if ((State.READY == state) && (MessageType.VOTE_COMMIT == e.type)) timeout();
		return;
	}

	@Override
	void internal(InternalEvent e) {
		switch (e.type) {
		case INITIALIZE:
			state = State.INIT;
			askNodeID = 1;
			calculateValue();
			break;
		case TIMEOUT:
			if (State.READY == e.state && State.READY == state) {
				// ask other nodes what happened
				if (askNodeID == id) askNodeID = (askNodeID + 1) % Interconnect.getParticipants().size();
				synchronized (events) {
					events.addLast(new SendEvent(this, Interconnect.getParticipants().get(askNodeID), currentCommitID,
							MessageType.DECISION));
				}
				break;
			}
			break;
		case RECOVER:
			state = preFailState;
			// we crashed in COMMIT / ABORT state, reinitialize node, as the last known
			// commit is already done
			if (State.COMMIT == state || State.ABORT == state) {
				synchronized (events) {
					events.addLast(
							new SendEvent(this, Interconnect.getCoordinator(), currentCommitID, MessageType.ACK));
					events.addLast(new InternalEvent(this, InternalEventType.INITIALIZE));
				}
				return;
			}
			// we crashed in INIT state, reinitialize the node
			if (State.INIT == state) {
				synchronized (events) {
					events.addLast(new InternalEvent(this, InternalEventType.INITIALIZE));
				}
				return;
			}
			// we crashed in READY state, ask other nodes what happened
			if (askNodeID == id) askNodeID = (askNodeID + 1) % Interconnect.getParticipants().size();
			synchronized (events) {
				events.addLast(new SendEvent(this, Interconnect.getParticipants().get(askNodeID), currentCommitID,
						MessageType.DECISION));
			}
			break;
		case CRASH:
			preFailState = state;
			state = State.CRASHED;
			synchronized (events) {
				events.clear();
			}
			break;
		case KILL:
			running = false;
			synchronized (events) {
				events.clear();
			}
			break;
		default:
			break;
		}
	}

	@Override
	void receive(ReceiveEvent e) {
		Message m = e.message;
		switch (m.type) {
		case VOTE_REQUEST:
			if (State.INIT == state) {
				currentCommitID = m.commitID;
				if (ThreadLocalRandom.current().nextDouble() < Overwatch.currentScenario.NODE_VOTE_ABORT_RATE) {
					synchronized (events) {
						events.addLast(new SendEvent(this, Interconnect.getCoordinator(), currentCommitID,
								MessageType.VOTE_ABORT));
					}
					abort();
				} else {
					proposedValue = (String) m.payload;

					state = State.READY;
					synchronized (events) {
						events.addLast(new SendEvent(this, Interconnect.getCoordinator(), currentCommitID,
								MessageType.VOTE_COMMIT));
					}
				}
			}
			break;
		case GLOBAL_ABORT:
			if ((State.READY == state) || (State.INIT == state)) abort();
			break;
		case GLOBAL_COMMIT:
			if (State.READY == state) commit();
			break;
		case DECISION:
			if (null == m.payload) {
				synchronized (events) {
					events.addLast(new SendEvent(this, m.sender, currentCommitID, MessageType.DECISION, state));
				}
				break;
			}
			if (!((State.READY == state) && (m.commitID == currentCommitID))) break;
			State s = (State) m.payload;
			if (State.COMMIT == s) {
				commit();
				break;
			}
			if (State.ABORT == s || State.INIT == s) {
				abort();
				break;
			}
			if (State.READY == s) {
				askNodeID = (askNodeID + 1) % Interconnect.getParticipants().size();
				if (askNodeID == id) askNodeID = (askNodeID + 1) % Interconnect.getParticipants().size();
				synchronized (events) {
					events.addLast(new SendEvent(this, Interconnect.getParticipants().get(askNodeID), currentCommitID,
							MessageType.DECISION));
				}
				break;
			}
			break;
		default:
			break;
		}

	}

	@Override
	void commit() {
		state = State.COMMIT;
		currentValue = proposedValue;

		log.put(currentCommitID, new Pair<>(State.COMMIT, new String(currentValue)));
		synchronized (events) {
			events.addLast(new SendEvent(this, Interconnect.getCoordinator(), currentCommitID, MessageType.ACK));
			events.addLast(new InternalEvent(this, InternalEventType.INITIALIZE));
		}
	}

	@Override
	void abort() {
		state = State.ABORT;

		log.put(currentCommitID, new Pair<>(State.ABORT, proposedValue));
		synchronized (events) {
			events.addLast(new SendEvent(this, Interconnect.getCoordinator(), currentCommitID, MessageType.ACK));
			events.addLast(new InternalEvent(this, InternalEventType.INITIALIZE));
		}
	}

	@Override
	void timeout() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				InternalEvent i = new InternalEvent(selfreference, InternalEventType.TIMEOUT, state);
				try {
					Thread.sleep(Overwatch.currentScenario.NODE_PARTICIPANT_TIMEOUT);
				} catch (InterruptedException e) {} finally {
					synchronized (events) {
						events.addLast(i);
						events.notify();
					}
				}

			}
		}).start();
	}

	@Override
	void tryCrash() {
		// don't crash an already crashed node
		if (State.CRASHED == state) return;
		if (Overwatch.currentScenario.NODE_CRASH_COUNT <= Interconnect.getCrashedNodes().size()) return;
		if (ThreadLocalRandom.current().nextDouble() >= Overwatch.currentScenario.NODE_CRASH_RATE) return;

		synchronized (events) {
			events.addFirst(new InternalEvent(selfreference, InternalEventType.CRASH));
			events.notify();
		}
		synchronized (Interconnect.getCrashedNodes()) {
			Interconnect.getCrashedNodes().add(selfreference);
		}

		new Thread(new Runnable() {
			@Override
			public void run() {
				InternalEvent i = new InternalEvent(selfreference, InternalEventType.RECOVER);
				int downTime = ThreadLocalRandom.current().nextInt(Overwatch.currentScenario.NODE_MIN_CRASH_TIME,
						Overwatch.currentScenario.NODE_MAX_CRASH_TIME + 1);
				try {
					Thread.sleep(downTime);
				} catch (InterruptedException e) {} finally {
					synchronized (selfreference.events) {
						selfreference.events.addFirst(i);
						selfreference.events.notify();
					}
					synchronized (Interconnect.getCrashedNodes()) {
						Interconnect.getCrashedNodes().remove(selfreference);
					}
				}
				Statistics.crash_avg_time.addAndGet(downTime);
				if (Statistics.crash_max_time.get() < downTime) {
					Statistics.crash_max_time.set(downTime);
					return;
				}
			}
		}).start();
		Statistics.total_crashes.incrementAndGet();

	}

	private void calculateValue() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				String val = Overwatch.currentScenario.local_value_pool.poll();
				if (null == val) return;

				SendEvent s = new SendEvent(selfreference, Interconnect.getCoordinator(), currentCommitID,
						MessageType.COMMIT_REQUEST, val);
				try {
					Thread.sleep(
							ThreadLocalRandom.current().nextInt(Overwatch.currentScenario.NODE_MIN_CALCULATION_TIME,
									Overwatch.currentScenario.NODE_MAX_CALCULATION_TIME + 1));
				} catch (InterruptedException e) {} finally {
					synchronized (events) {
						events.addLast(s);
						events.notify();
					}
				}
			}
		}).start();
	}
}
