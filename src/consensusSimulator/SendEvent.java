package consensusSimulator;

import java.util.LinkedList;
import java.util.List;
import consensusSimulator.Message.MessageType;

public class SendEvent extends AbstractEvent {

	AbstractNode		sender;
	List<AbstractNode>	receivers;
	int					commitID;
	MessageType			type;
	Object				payload;

	public SendEvent(AbstractNode sender, AbstractNode receiver, int commitID, MessageType type, Object payload) {
		super(sender, EventType.SEND);
		this.sender = sender;
		this.commitID = commitID;
		this.type = type;
		this.payload = payload;

		this.receivers = new LinkedList<AbstractNode>();
		receivers.add(receiver);
	}

	public SendEvent(AbstractNode sender, AbstractNode receiver, int commitID, MessageType type) {
		this(sender, receiver, commitID, type, null);
	}

	public SendEvent(AbstractNode sender, List<AbstractNode> receivers, int commitID, MessageType type,
			Object payload) {
		super(sender, EventType.SEND);
		this.sender = sender;
		this.receivers = receivers;
		this.commitID = commitID;
		this.type = type;
		this.payload = payload;
	}

	public SendEvent(AbstractNode sender, List<AbstractNode> receivers, int commitID, MessageType type) {
		this(sender, receivers, commitID, type, null);
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(super.toString());
		s.append(String.format("%-14s %6d %4d ", type, commitID, sender.id));

		if (MessageType.COMMIT_REQUEST == type) s.append((String) payload);
		if (MessageType.DECISION == type) s.append((AbstractNode.State) payload);
		if (MessageType.VOTE_REQUEST == type) s.append((String) payload);
		if (null == payload) s.append(' ');

		s.append("[");
		for (AbstractNode receiver : receivers) {
			s.append(receiver.id);
			s.append(", ");
		}
		if (!receivers.isEmpty()) {
			s.deleteCharAt(s.length() - 1);
			s.deleteCharAt(s.length() - 1);
		}
		s.append(']');
		return s.toString();
	}

}
