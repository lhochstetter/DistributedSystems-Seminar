package consensusSimulator;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import consensusSimulator.InternalEvent.InternalEventType;
import consensusSimulator.Message.MessageType;

/**
 * Class to mimic communication channels between nodes by providing an
 * thread-safe "mail box" per node
 */
public abstract class Interconnect {
	private static List<Thread>								threads			= new ArrayList<Thread>();
	private static Map<AbstractNode, Deque<AbstractEvent>>	addressbook		= new LinkedHashMap<AbstractNode, Deque<AbstractEvent>>();
	private static List<AbstractNode>						crashedNodes	= new ArrayList<AbstractNode>();
	private static List<AbstractNode>						participants	= new ArrayList<AbstractNode>();
	private static AbstractNode								coordinator;

	public static void send(AbstractNode sender, Iterable<AbstractNode> receivers, int commitID, MessageType type,
			Object payload) {
		for (AbstractNode n : receivers) {
			if (null == n) continue;
			if (crashedNodes.contains(n)) {
				Statistics.message_dropped.incrementAndGet();
				continue;
			}
			new Thread(new Runnable() {
				public void run() {
					ReceiveEvent r = new ReceiveEvent(new Message(sender, n, commitID, type, payload));
					int travelTime = ThreadLocalRandom.current().nextInt(Overwatch.currentScenario.MSG_MIN_DELAY,
							Overwatch.currentScenario.MSG_MAX_DELAY + 1);
					Deque<AbstractEvent> eventQueue = addressbook.get(n);
					try {
						Thread.sleep(travelTime);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						synchronized (eventQueue) {
							eventQueue.addLast(r);
							eventQueue.notify();
						}
					}
					Statistics.message_count.incrementAndGet();
					Statistics.message_avg_delay.addAndGet(travelTime);
					if (Statistics.message_max_delay.get() < travelTime) Statistics.message_max_delay.set(travelTime);
					return;
				}
			}).start();
		}
		return;
	}

	public static void setup(int nodeCount) {
		AbstractNode node = new Coordinator(0, 0);
		addressbook.put(node, node.events);
		coordinator = node;
		threads.add(new Thread(node));

		for (int i = 1; i < nodeCount; i++) {
			node = new Participant(i, 0);
			addressbook.put(node, node.events);
			participants.add(node);
			threads.add(new Thread(node));
		}
	}

	public static void reset() {
		threads.clear();
		addressbook.clear();
		crashedNodes.clear();
		participants.clear();
		coordinator = null;
	}

	public static void start() {
		for (Thread t : threads) {
			t.start();
		}

		for (AbstractNode n : Interconnect.getParticipants()) {
			synchronized (n.events) {
				n.events.addFirst(new InternalEvent(n, InternalEventType.INITIALIZE));
				n.events.notify();
			}
		}
	}

	public static void terminateGraceful() {
		for (AbstractNode n : addressbook.keySet()) {
			synchronized (addressbook.get(n)) {
				addressbook.get(n).addLast(new InternalEvent(n, InternalEventType.KILL));
				addressbook.get(n).notify();
			}
		}

		for (Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static Set<AbstractNode> getNodes() {
		return addressbook.keySet();
	}

	public static AbstractNode getCoordinator() {
		return coordinator;
	}

	public static List<AbstractNode> getParticipants() {
		return participants;
	}

	public static List<AbstractNode> getCrashedNodes() {
		return crashedNodes;
	}

}