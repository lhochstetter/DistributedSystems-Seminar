package consensusSimulator;

public abstract class AbstractEvent {

	enum EventType {
		INTERNAL, SEND, RECEIVE
	};

	EventType		type;
	AbstractNode	node;

	public AbstractEvent(AbstractNode node, EventType type) {
		this.node = node;
		this.type = type;
	}

	@Override
	public String toString() {
		if (0 == node.id) return String.format("%-5s | %6d | %-18s %18d | %-9s | %-10s | ", "COORD",
				node.currentCommitID, ((Coordinator) node).pendingCommits.peekFirst(),
				((Coordinator) node).pendingCommits.size(), type, node.state);
		return String.format("%5d | %6d | %-18s %-18s | %-9s | %-10s | ", node.id, node.currentCommitID,
				((Participant) node).proposedValue, ((Participant) node).currentValue, type, node.state);
	}
}
