package consensusSimulator;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * This class encapsulates different settings for easier setup of different
 * scenarios <br/>
 * <br/>
 * Note: the size of DESIRED_COMMITS should not exceed 13 as there are currently
 * only 13 values
 */
public class Scenario {

	private static String[] global_value_pool = new String[] { "cupcake", "donut", "eclair", "froyo", "gingerbread",
			"honeycomb", "ice cream sandwich", "jelly bean", "kitkat", "lollipop", "marshmallow", "nougat", "oreo" };

	public ConcurrentLinkedQueue<String> local_value_pool = new ConcurrentLinkedQueue<String>();

	/* general parameters */
	public int	MAX_COMMIT_TRIES;
	public int	DESIRED_COMMITS;

	/* node parameters */
	public int	NODE_COORDINATOR_TIMEOUT;
	public int	NODE_PARTICIPANT_TIMEOUT;

	public double	NODE_VOTE_ABORT_RATE;
	public int		NODE_MIN_CALCULATION_TIME;
	public int		NODE_MAX_CALCULATION_TIME;

	public double	NODE_CRASH_RATE;
	public int		NODE_CRASH_COUNT;
	public int		NODE_MIN_CRASH_TIME;
	public int		NODE_MAX_CRASH_TIME;

	/* message parameters */
	public int	MSG_MIN_DELAY;
	public int	MSG_MAX_DELAY;

	public Scenario() {}

	public void generateValuePool() {
		if (global_value_pool.length < DESIRED_COMMITS) {
			System.err.println(
					"ERROR: there are not enough values in the value pool to create the desired amount of commits");
			return;
		}
		for (int i = 0; i < DESIRED_COMMITS; i++)
			this.local_value_pool.add(global_value_pool[i]);
		return;
	}

	@Override
	public String toString() {
		// prettify output with String.format();
		// https://stackoverflow.com/questions/26576909/how-to-format-string-output-so-that-columns-are-evenly-centered
		StringBuilder sb = new StringBuilder();
		sb.append("SCENARIO\n");
		sb.append(String.format("%-28s%d\n", "MAX_COMMIT_TRIES", MAX_COMMIT_TRIES));
		sb.append(String.format("%-28s%d\n", "DESIRED_COMMITS", DESIRED_COMMITS));
		sb.append(String.format("%-28s%d\n", "NODE_COORDINATOR_TIMEOUT", NODE_COORDINATOR_TIMEOUT));
		sb.append(String.format("%-28s%d\n", "NODE_PARTICIPANT_TIMEOUT", NODE_PARTICIPANT_TIMEOUT));
		sb.append(String.format("%-28s%f\n", "NODE_VOTE_ABORT_RATE", NODE_VOTE_ABORT_RATE));
		sb.append(String.format("%-28s%d\n", "MSG_MIN_DELAY", MSG_MIN_DELAY));
		sb.append(String.format("%-28s%d\n", "MSG_MAX_DELAY", MSG_MAX_DELAY));
		sb.append(String.format("%-28s", "VALUE_POOL"));
		sb.append(local_value_pool);

		return sb.toString();
	}
}