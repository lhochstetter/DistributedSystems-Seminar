package consensusSimulator;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public abstract class Statistics {
	static public AtomicLong	total_commits		= new AtomicLong(0);
	static public AtomicLong	successful_commits	= new AtomicLong(0);
	static public AtomicLong	aborted_commits		= new AtomicLong(0);

	static public AtomicLong	total_crashes	= new AtomicLong(0);
	static public AtomicInteger	crash_max_time	= new AtomicInteger(0);
	static public AtomicLong	crash_avg_time	= new AtomicLong(0);

	static public AtomicLong	message_count		= new AtomicLong(0);
	static public AtomicLong	message_dropped		= new AtomicLong(0);
	static public AtomicInteger	message_max_delay	= new AtomicInteger(0);
	static public AtomicLong	message_avg_delay	= new AtomicLong(0);

	static public void reset() {
		total_commits.set(0);
		successful_commits.set(0);
		aborted_commits.set(0);
		
		total_crashes.set(0);
		crash_max_time.set(0);
		crash_avg_time.set(0);
		
		message_count.set(0);
		message_dropped.set(0);
		message_max_delay.set(0);
		message_avg_delay.set(0);
	}
	
	static public void print() {
		StringBuilder sb = new StringBuilder();
		sb.append("STATISTICS\n");
		sb.append(String.format("%-30s %6d\n", "Total number of commits", total_commits.get()));
		sb.append(String.format("%-30s %6d\n", "Successfull commits", successful_commits.get()));
		sb.append(String.format("%-30s %6d\n", "Aborted commits", aborted_commits.get()));
		sb.append('\n');
		sb.append(String.format("%-30s %6d\n", "Total number of crashes", total_crashes.get()));
		sb.append(String.format("%-30s %6d\n", "Maximum down time", crash_max_time.get()));
		if (0 != total_crashes.get())
			sb.append(String.format("%-30s %6d\n", "Average down time", (crash_avg_time.get() / total_crashes.get())));
		else
			sb.append(String.format("%-30s %6s\n", "Average down time", "N/A"));
		sb.append('\n');
		sb.append(String.format("%-30s %6d\n", "Total number of messages", message_count.get()));
		sb.append(String.format("%-30s %6d\n", "Number of messages dropped", message_dropped.get()));
		sb.append(String.format("%-30s %6d\n", "Maximum message delay", message_max_delay.get()));
		if (0 != message_count.get())
			sb.append(String.format("%-30s %6d\n", "Average message delay",
					(message_avg_delay.get() / message_count.get())));
		else
			sb.append(String.format("%-30s %6s\n", "Average message delay", "N/A"));

		System.out.println(sb);
	}
}
