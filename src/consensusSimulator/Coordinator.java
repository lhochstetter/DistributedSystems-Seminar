package consensusSimulator;

import java.util.LinkedList;
import consensusSimulator.InternalEvent.InternalEventType;
import consensusSimulator.Message.MessageType;

public class Coordinator extends AbstractNode {

	boolean	votedAbort	= false;
	int		votedCommit	= 0;

	LinkedList<String> pendingCommits;

	public Coordinator(int id, int currentCommitID) {
		super(id, currentCommitID);
		pendingCommits = new LinkedList<>();
		state = State.INIT;
	}

	@Override
	void send(SendEvent e) {
		Interconnect.send(e.sender, e.receivers, e.commitID, e.type, e.payload);
		if (State.WAIT == state || State.COMMIT == state || State.ABORT == state) timeout();
		return;
	}

	@Override
	void internal(InternalEvent e) {
		switch (e.type) {
		// ignore timeouts if they belonged to another state
		case TIMEOUT:
			if (State.WAIT == e.state && State.WAIT == state) {
				abort();
				break;
			}
			if (State.ABORT == e.state && State.ABORT == state) {
				currentCommitID++;
				synchronized (Statistics.total_commits) {
					Statistics.total_commits.incrementAndGet();
					Statistics.total_commits.notify();
				}
				Statistics.aborted_commits.incrementAndGet();

				if (!(pendingCommits.isEmpty())) {
					synchronized (events) {
						events.addLast(new InternalEvent(this, InternalEventType.INITIALIZE));
					}
				}
				state = State.INIT;
				break;
			}
			if (State.COMMIT == e.state && State.COMMIT == state) {
				currentCommitID++;
				synchronized (Statistics.total_commits) {
					Statistics.total_commits.incrementAndGet();
					Statistics.total_commits.notify();
				}
				Statistics.successful_commits.incrementAndGet();

				pendingCommits.removeFirst();
				if (!(pendingCommits.isEmpty())) {
					synchronized (events) {
						events.addLast(new InternalEvent(this, InternalEventType.INITIALIZE));
					}
				}
				state = State.INIT;
				break;
			}
			break;
		case KILL:
			running = false;
			synchronized (events) {
				events.clear();
			}
			break;
		case INITIALIZE:
			synchronized (events) {
				events.addLast(new SendEvent(this, Interconnect.getParticipants(), currentCommitID,
						MessageType.VOTE_REQUEST, pendingCommits.getFirst()));
			}
			state = State.WAIT;
			break;
		default:
			break;
		}

	}

	@Override
	void receive(ReceiveEvent e) {
		Message m = e.message;
		if (MessageType.COMMIT_REQUEST == m.type) {
			pendingCommits.add((String) m.payload);
			if (State.INIT == state) {
				synchronized (events) {
					events.addLast(new InternalEvent(this, InternalEventType.INITIALIZE));
				}
			}
			return;
		}
		if ((MessageType.VOTE_ABORT == m.type) && (State.WAIT == state)) {
			// discard older / delayed messages
			if (m.commitID < currentCommitID) return;
			votedAbort = true;
			abort();
			return;
		}
		if ((MessageType.VOTE_COMMIT == m.type) && (State.WAIT == state)) {
			// discard older / delayed messages
			if (m.commitID < currentCommitID) return;
			votedCommit += 1;
			if (Interconnect.getParticipants().size() == votedCommit) commit();
			return;
		}
		if ((MessageType.ACK == m.type) && (State.ABORT == state || State.COMMIT == state)) {
			// discard older / delayed messages
			if (m.commitID < currentCommitID) return;
			return;
		}
	}

	@Override
	void commit() {
		votedCommit = 0;
		votedAbort = false;

		state = State.COMMIT;
		synchronized (events) {
			events.addLast(
					new SendEvent(this, Interconnect.getParticipants(), currentCommitID, MessageType.GLOBAL_COMMIT));
		}
		log.put(currentCommitID, new Pair<>(State.COMMIT, pendingCommits.getFirst()));
	}

	@Override
	void abort() {
		votedCommit = 0;
		votedAbort = false;

		state = State.ABORT;
		synchronized (events) {
			events.addLast(
					new SendEvent(this, Interconnect.getParticipants(), currentCommitID, MessageType.GLOBAL_ABORT));
		}

		log.put(currentCommitID, new Pair<>(State.ABORT, pendingCommits.getFirst()));
	}

	@Override
	void timeout() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				InternalEvent t = new InternalEvent(selfreference, InternalEventType.TIMEOUT, state);
				try {
					Thread.sleep(Overwatch.currentScenario.NODE_COORDINATOR_TIMEOUT);
				} catch (InterruptedException e) {} finally {
					synchronized (events) {
						events.addLast(t);
						events.notify();
					}
				}

			}
		}).start();
	}

	@Override
	void tryCrash() {
		// this implementation assumes a immortal coordinator;
		return;
	}

}
