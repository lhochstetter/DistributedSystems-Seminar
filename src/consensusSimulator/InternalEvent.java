package consensusSimulator;

import consensusSimulator.AbstractNode.State;

public class InternalEvent extends AbstractEvent {

	enum InternalEventType {
		TIMEOUT, // used to simulate a timeout
		RESET, // prepare the next commit, used to leave ABORT / COMMIT states
		KILL, // used for a graceful shutdown
		INITIALIZE, // used to start 2PC
		CRASH, // used to crash the node
		RECOVER, // used to recover the node
		VALUE // used to simulate the computation of a value eligible for committing
	};

	InternalEventType	type;
	State				state;

	public InternalEvent(AbstractNode node, InternalEventType type) {
		super(node, EventType.INTERNAL);
		this.type = type;
		this.state = State.NIL;

	}

	public InternalEvent(AbstractNode node, InternalEventType type, State state) {
		super(node, EventType.INTERNAL);
		this.type = type;
		this.state = state;

	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(super.toString());
		s.append(String.format("%-10s", type));

		if ((InternalEventType.INITIALIZE == type) && (0 == node.id)) s.append(((Coordinator) node).pendingCommits);
		if ((InternalEventType.TIMEOUT == type) && (State.NIL != state)) s.append(state);

		return s.toString();
	}

}
