package consensusSimulator;

import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Abstract class to hold fields and methods which a normal node and a
 * coordinator node have in common
 */
public abstract class AbstractNode implements Runnable {

	class Pair<A, B> {
		public final A	a;
		public final B	b;

		public Pair(A a, B b) {
			this.a = a;
			this.b = b;
		}
	}

	enum State {
		NIL, CRASHED, INIT, WAIT, READY, ABORT, COMMIT;
	}

	int				id;
	boolean			running;
	int				currentCommitID;
	AbstractNode	selfreference;

	State								state;
	Deque<AbstractEvent>				events;
	Map<Integer, Pair<State, Object>>	log;

	public AbstractNode(int id, int currentCommitID) {
		selfreference = this;
		running = true;
		this.id = id;
		this.currentCommitID = currentCommitID;
		log = new LinkedHashMap<Integer, Pair<State, Object>>();
		events = new LinkedList<>();
	}

	void processEvent(AbstractEvent e) {
		System.out.println(e.toString());
		switch (e.type) {
		case INTERNAL:
			internal((InternalEvent) e);
			break;
		case SEND:
			send((SendEvent) e);
			break;
		case RECEIVE:
			receive((ReceiveEvent) e);
			break;
		default:
			System.err.println("ERROR: unknown event: " + e.type);
			break;
		}
	}

	abstract void internal(InternalEvent e);

	abstract void send(SendEvent e);

	abstract void receive(ReceiveEvent e);

	abstract void commit();

	abstract void abort();

	abstract void timeout();

	abstract void tryCrash();

	@Override
	public void run() {
		AbstractEvent e;
		while (running) {
			tryCrash();
			synchronized (events) {
				while (events.isEmpty()) {
					try {
						events.wait();
					} catch (InterruptedException e1) {}
				}
				e = events.pollFirst();
			}
			if (null == e) continue;
			processEvent(e);
		}
	}

}
